package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Optional;

import java.time.LocalTime;

public class DepartureTest {
  private Departure osloBound;
  private Departure trondheimBound;

  @BeforeEach
  void setUp() {
    osloBound = new Departure(LocalTime.of(12, 0), "F1", "Oslo S");
    osloBound.setDelay(LocalTime.of(0, 15));
    trondheimBound = new Departure(LocalTime.of(14, 30), "F2", "Trondheim S");
    trondheimBound.setTrack(5);
  }

  @Test
  void constructor() {
    assertDoesNotThrow(() -> new Departure(LocalTime.of(12, 0), "F1", "Oslo S"));
    assertThrows(IllegalArgumentException.class, () -> new Departure(null, "F1", "Oslo S"));
    assertThrows(IllegalArgumentException.class, () -> new Departure(LocalTime.of(12, 0), null, "Oslo S"));
    assertThrows(IllegalArgumentException.class, () -> new Departure(LocalTime.of(12, 0), "F1", null));
  }

  @Test
  void getTime() {
    assertEquals(LocalTime.of(12, 0), osloBound.getTime());
    assertEquals(LocalTime.of(14, 30), trondheimBound.getTime());
  }

  @Test
  void getRealTime() {
    assertEquals(LocalTime.of(12, 15), osloBound.getRealTime());
    assertEquals(LocalTime.of(14, 30), trondheimBound.getRealTime());
  }

  @Test
  void getLine() {
    assertEquals("F1", osloBound.getLine());
    assertEquals("F2", trondheimBound.getLine());
  }

  @Test
  void getDestination() {
    assertEquals("Oslo S", osloBound.getDestination());
    assertEquals("Trondheim S", trondheimBound.getDestination());
  }

  @Test
  void getTrack() {
    assertTrue(osloBound.getTrack().isEmpty());
    assertEquals(5, trondheimBound.getTrack().get());
  }

  @Test
  void setTrack() {
    osloBound.setTrack(1);
    trondheimBound.setTrack(2);
    assertEquals(1, osloBound.getTrack().get());
    assertEquals(2, trondheimBound.getTrack().get());
  }

  @Test
  void resetTrack() {
    trondheimBound.resetTrack();
    assertTrue(trondheimBound.getTrack().isEmpty());
  }

  @Test
  void setDelay() {
    osloBound.setDelay(LocalTime.of(0, 30));
    trondheimBound.setDelay(LocalTime.of(1, 0));
    assertEquals(LocalTime.of(0, 30), osloBound.getDelay());
    assertEquals(LocalTime.of(15, 30), trondheimBound.getRealTime());
    assertThrows(IllegalArgumentException.class, () -> osloBound.setDelay(null));
  }
}
