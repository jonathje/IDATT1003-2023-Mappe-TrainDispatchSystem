package edu.ntnu.stud.validation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParameterTest {
  @Test
  public void notBlank() {
    assertDoesNotThrow(() -> Parameter.notBlank("foo", "bar"));
    assertThrows(IllegalArgumentException.class, () -> Parameter.notBlank("", "foo"));

    assertEquals("bar cannot be blank",
            assertThrows(
                    IllegalArgumentException.class,
                    () -> Parameter.notBlank("", "bar")
            ).getMessage()
    );
  }

  @Test
  public void positive() {
    assertDoesNotThrow(() -> Parameter.positive(1, "foo"));
    assertThrows(IllegalArgumentException.class, () -> Parameter.positive(0, "foo"));
    assertThrows(IllegalArgumentException.class, () -> Parameter.positive(-1, "foo"));

    assertEquals("foo must be positive",
            assertThrows(
                    IllegalArgumentException.class,
                    () -> Parameter.positive(-1, "foo")
            ).getMessage()
    );
  }

  @Test
    public void notNull() {
        assertDoesNotThrow(() -> Parameter.notNull("foo", "bar"));
        assertThrows(IllegalArgumentException.class, () -> Parameter.notNull(null, "foo"));

        assertEquals("bar cannot be null",
                assertThrows(
                        IllegalArgumentException.class,
                        () -> Parameter.notNull(null, "bar")
                ).getMessage()
        );
    }
}
