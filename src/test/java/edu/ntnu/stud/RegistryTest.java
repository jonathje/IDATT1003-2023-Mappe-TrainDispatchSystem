package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.Map;

public class RegistryTest {
  private Registry reg;
  private Departure osloBound;
  private Departure trondheimBound;
  private Departure bergenBound;
  private Departure stavangerBound;

  @BeforeEach
  void setUp() {
    reg = new Registry();
    osloBound = new Departure(LocalTime.of(12, 0), "F1", "Oslo S");
    trondheimBound = new Departure(LocalTime.of(14, 30), "F2", "Trondheim S");
    bergenBound = new Departure(LocalTime.of(17, 0), "F3", "Bergen S");
    stavangerBound = new Departure(LocalTime.of(19, 30), "F4", "Stavanger S");

    reg.add(1, osloBound);
    reg.add(2, trondheimBound);
    reg.add(3, bergenBound);
  }

  @Test
  void constructor() {
    assertDoesNotThrow(Registry::new);
  }

  @Test
  void remove() {
    assertEquals(osloBound, reg.remove(1).orElseThrow());
    assertTrue(reg.remove(1).isEmpty());
  }

  @Test
  void fromMapPair() {
    var mapEntry = Map.entry(1, osloBound);
    var regEntry = Registry.Entry.fromMapPair(mapEntry);
    assertEquals(1, regEntry.number());
    assertEquals(osloBound, regEntry.departure());
  }

  @Test
  void add() {
    reg.add(4, stavangerBound);
    assertEquals(stavangerBound, reg.getDeparture(4).get());
  }

  @Test
  void numbersInUse() {
    var numbers = reg.numbersInUse();
    assertEquals(3, numbers.size());
    assertTrue(numbers.contains(1));
    assertTrue(numbers.contains(2));
    assertTrue(numbers.contains(3));
    assertFalse(numbers.contains(4));
  }

  @Test
  void getDeparture() {
    assertEquals(osloBound, reg.getDeparture(1).get());
    assertTrue(reg.getDeparture(4).isEmpty());
  }

  @Test
  void withDestination() {
    var query = reg.withDestination("Oslo S");
    assertEquals(1, query.length);
    assertEquals(osloBound, query[0].departure());
    var query2 = reg.withDestination("Trondheim S");
    assertEquals(1, query2.length);
    assertEquals(trondheimBound, query2[0].departure());
    var emptyQuery = reg.withDestination("Stavanger S");
    assertEquals(0, emptyQuery.length);
  }

  @Test
  void withLine() {
    var query = reg.withLine("F1");
    assertEquals(1, query.length);
    assertEquals(osloBound, query[0].departure());
    var query2 = reg.withLine("F2");
    assertEquals(1, query2.length);
    assertEquals(trondheimBound, query2[0].departure());
    var emptyQuery = reg.withLine("F4");
    assertEquals(0, emptyQuery.length);
  }

  @Test
  void timeLinePairs() {
    var pairs = reg.timeLinePairs();
    assertEquals(3, pairs.size());
    assertEquals(osloBound.getLine(), pairs.get(LocalTime.of(12, 0)));
    assertEquals(trondheimBound.getLine(), pairs.get(LocalTime.of(14, 30)));
    assertEquals(bergenBound.getLine(), pairs.get(LocalTime.of(17, 0)));
  }

  @Test
  void afterOrAt() {
    var query = reg.afterOrAt(LocalTime.of(14, 30));
    assertEquals(2, query.length);
    assertEquals(trondheimBound, query[0].departure());
    assertEquals(bergenBound, query[1].departure());
  }

  @Test
  void byTime() {
    var query = reg.byTime();
    assertEquals(osloBound, query[0].departure());
    assertEquals(trondheimBound, query[1].departure());
    assertEquals(bergenBound, query[2].departure());
  }
}
