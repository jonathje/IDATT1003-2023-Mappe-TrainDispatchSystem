package edu.ntnu.stud;

import edu.ntnu.stud.validation.Parameter;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Represents a registry of train departures from a station.
 * It acts as an interface through which to read and modify the
 * set of departures on which the train dispatch application operates.
 */
public class Registry {
  private final HashMap<Integer, Departure> departures;

  private Stream<Map.Entry<Integer, Departure>> departureStream() {
    return departures.entrySet().stream().filter(entry -> {
      var dep = entry.getValue();
      return !(dep.getRealTime().isBefore(dep.getTime()));
    });
  }

  private static Entry[] mapToEntries(Stream<Map.Entry<Integer, Departure>> stream) {
    return stream.map(Entry::fromMapPair).toArray(Entry[]::new);
  }

  /**
   * Constructs a new, empty registry.
   * If you want to populate the registry with departures, call {@link #add(int, Departure)}
   * after constructing the registry.
   */
  public Registry() {
    this.departures = new HashMap<>();
  }

  /**
   * Removes the departure with the given departure number from the registry.
   *
   * @param id The departure number of the departure to be removed
   * @return The departure which was removed, if it existed
   */
  public Optional<Departure> remove(int id) {
    return Optional.ofNullable(this.departures.remove(id));
  }

  private HashMap<Integer, Departure> getDepartures() {
    return departures;
  }

  /**
   * A record which represents an entry in the departure registry.
   * It consists of a departure and its departure number, which are available
   * through their associated methods.
   *
   * @param number The departure number
   * @param departure The departure itself
   */
  public record Entry(int number, Departure departure) {
    /**
     * Constructs a new entry from a map entry.
     *
     * @param entry The map entry to construct the entry from
     * @return A new entry with the same departure number and departure as the given map entry
     * @throws IllegalArgumentException If the given entry is null
     */
    public static Entry fromMapPair(Map.Entry<Integer, Departure> entry) {
      Parameter.notNull(entry, "entry");
      return new Entry(entry.getKey(), entry.getValue());
    }
  }

  /**
   * Adds a departure to the registry, with the given departure number.
   *
   * @param number The number to be associated with the departure
   * @param departure The departure to be added to the registry
   * @throws IllegalStateException If the given number is already
   *     associated with a departure in the registry
   */
  public void add(int number, Departure departure) {
    if (departures.containsKey(number)) {
      throw new IllegalStateException("Departure " + number + " already exists");
    }
    departures.put(number, departure);
  }

  /**
   * The set of all departure numbers currently in use.
   *
   * @return A set of all departure numbers currently in use
   */
  public Set<Integer> numbersInUse() {
    return departures.keySet();
  }

  /**
   * Returns the departure given by its departure number, if it exists.
   *
   * @param id The departure number of the requested departure
   * @return An {@link Optional} containing the departure associated
   *     with the given number, or an empty {@link Optional}
   *     if the number has no associated departure
   */
  public Optional<Departure> getDeparture(int id) {
    var out = departures.get(id);
    return Optional.ofNullable(out);
  }

  /**
   * Gets all registered departures to the destination specified.
   *
   * <p>Departures are in the form of a {@link Registry.Entry},
   * which contains both the departure number and the {@link Departure} itself.
   *
   * <p>The order of the returned collection is not specified.
   *
   * @param destination The destination to filter by
   * @return All registered departures to the destination specified
   * @throws IllegalArgumentException If the given destination is null
   */
  public Entry[] withDestination(String destination) {
    Parameter.notNull(destination, "destination");

    var out = departureStream()
            .filter(entry -> entry.getValue().getDestination().equals(destination));
    return mapToEntries(out);
  }

  /**
   * Gets all registered departures on the line specified.
   *
   * <p>Departures are in the form of a {@link Registry.Entry},
   * which contains both the departure number and the {@link Departure} itself.
   *
   * <p>The order of the returned collection is not specified.
   *
   * @param line The line to filter by
   * @return All registered departures on the line specified
   * @throws IllegalArgumentException If the given line is null
   */
  public Entry[] withLine(String line) {
    Parameter.notNull(line, "line");

    var out = departureStream()
            .filter(entry -> entry.getValue().getLine().equals(line));
    return mapToEntries(out);
  }

  /**
   * Returns the times of each departure matched to their line.
   *
   * @return A HashMap of times to lines
   */
  public HashMap<LocalTime, String> timeLinePairs() {
    return departureStream()
            .collect(HashMap::new, (map, entry) -> {
              var dep = entry.getValue();
              map.put(dep.getTime(), dep.getLine());
            }, HashMap::putAll);
  }

  /**
   * Gets all registered departures which have not left according to the time specified.
   * Delays are taken into account when deciding whether a departure has left or not.
   *
   * <p>Departures are in the form of a {@link Registry.Entry},
   * which contains both the departure number and the {@link Departure} itself.
   *
   * <p>The returned collection is sorted by time and destination.
   *
   * @param time The time to filter by
   * @return All registered departures with a departure time after or at the specified time
   * @throws IllegalArgumentException If the given time is null
   */
  public Entry[] afterOrAt(LocalTime time) {
    Parameter.notNull(time, "time");

    var comparison = Comparator.comparing(
            (Map.Entry<Integer, Departure> entry) -> entry.getValue().getTime()
    ).thenComparing(entry -> entry.getValue().getDestination());
    var out = departureStream()
            .filter(entry -> {
              var realTime = entry.getValue().getRealTime();
              return realTime.isAfter(time) || realTime.equals(time);
            })
            .sorted(comparison);
    return mapToEntries(out);
  }

  /**
   * Gets all departures, sorted first by time, then destination.
   *
   * <p>Departures are in the form of a {@link Registry.Entry},
   * which contains both the departure number and the {@link Departure} itself.
   *
   * @return All departures, sorted first by time, then destination
   */
  public Entry[] byTime() {
    var comparison = Comparator.comparing(
                            (Map.Entry<Integer, Departure> entry) -> entry.getValue().getTime()
                    ).thenComparing(entry -> entry.getValue().getDestination());
    var out = departureStream()
            .sorted(comparison);
    return mapToEntries(out);
  }
}


