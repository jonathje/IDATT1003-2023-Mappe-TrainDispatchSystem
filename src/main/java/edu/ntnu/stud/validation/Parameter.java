package edu.ntnu.stud.validation;

/**
 * Utility for validating method parameters.
 * Methods in this class throw exceptions by default if their arguments are invalid.
 *
 * <p>Example usage:
 *
 * <pre>
 * public void setFoo(String foo) {
 *   Parameter.notBlank(foo, "foo");
 *   this.foo = foo;
 * }
 * </pre>
 */
public class Parameter {
  /**
   * Throws an exception if the argument string is blank.
   *
   * @param param the parameter to check
   * @param name the name of the parameter for use with the exception message
   * @throws IllegalArgumentException if the parameter is blank
   */
  public static void notBlank(String param, String name) {
    if (param.isBlank()) {
      throw new IllegalArgumentException(name + " cannot be blank");
    }
  }

  /**
   * Throws an exception if the argument integer is negative or zero.
   *
   * @param param the parameter to check
   * @param name the name of the parameter for use with the exception message
   * @throws IllegalArgumentException if the parameter is negative or zero
   */
  public static void positive(int param, String name) {
    if (param < 1) {
      throw new IllegalArgumentException(name + " must be positive");
    }
  }

  /**
   * Throws an exception if the argument is null.
   *
   * @param param the parameter to check
   * @param name the name of the parameter for use with the exception message
   * @throws IllegalArgumentException if the parameter is null
   */
  public static void notNull(Object param, String name) {
    if (param == null) {
      throw new IllegalArgumentException(name + " cannot be null");
    }
  }
}
