package edu.ntnu.stud;

import edu.ntnu.stud.validation.Parameter;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;
import java.util.Scanner;

/**
 * Responsible for all direct interaction with the user.
 * No printing or scanning is done except through this class.
 */
public final class Ui {
  private final Scanner scanner;

  private static final String CANCEL = "cancel";

  /**
   * Creates a new Ui instance.
   */
  public Ui() {
    this.scanner = new Scanner(System.in);
  }

  private Scanner getScanner() {
    return this.scanner;
  }

  private String getLine() {
    var line = getScanner().nextLine();
    if (line.equals(CANCEL)) {
      throw new UserCancelException();
    }
    return line;
  }

  /**
   * Displays a message to the user, showing how to cancel commands together with a custom message.
   * Mostly used when the user entered an invalid input,
   * which might indicate that they're looking for a way to cancel.
   * If null is passed, only the cancel info is printed.
   */
  public void printCancelInfoWith(String message) {
    if (message != null) {
      System.out.println(message);
    }
    System.out.println("(use '" + CANCEL + "' if you want to abort the command)");
  }

  /**
   * Prints a list of commands as available to the user. If null is passed, nothing is printed.
   *
   * @param commands the commands to print
   */
  public void printCommands(Command[] commands) {
    if (commands == null) {
      return;
    }
    
    System.out.println("Available commands:\n");
    System.out.println("\tEnter '" + CANCEL + "' to cancel any running command\n");
    for (var command : commands) {
      System.out.println("\t'" + command.identifier() + "': " + command.description());
    }
  }

  /**
   * Awaits user input, and returns the command that matches the input.
   *
   * @param commands the commands to match against
   * @return the command that matches the user input
   * @throws IllegalArgumentException if the argument is null
   */
  public Command promptCommand(Command[] commands) {
    Parameter.notNull(commands, "commands");
    
    while (true) {
      String prompt = null;
      while (prompt == null) {
        try {
          prompt = getLine();
        } catch (UserCancelException e) {
          System.out.println("No command is running");
        }
      }
      for (var command : commands) {
        if (command.identifier().equals(prompt)) {
          return command;
        }
      }
      System.out.println("Invalid command: " + prompt);
    }
  }

  /**
   * Prints the given departure to the user in a standard format.
   * If null is passed, nothing is printed.
   */
  public void printDeparture(Registry.Entry entry) {
    if (entry == null) {
      return;
    }

    var departure = entry.departure();
    var number = entry.number();
    var track = departure.getTrack();
    var delay = departure.getDelay().toString();
    System.out.println("\nDeparture " + number + ":"
        + "\n\tTime: " + departure.getTime()
        + "\n\tLine: " + departure.getLine()
        + "\n\tDestination: " + departure.getDestination()
        + "\n\t" + (track.map(integer -> "Track: " + integer).orElse("No track set"))
        + "\n\t" + (delay.equals("00:00") ? "No delay" : "Delay: " + delay)
    );
  }

  /**
   * Prints the formatted departure table to the user,
   * with only departures after or at the given time
   * If registry is null, nothing is printed.
   * If time is null, the time is not printed,
   * and all departures are printed as if the time was 00:00.
   */
  public void printDepartureList(Registry registry, LocalTime time) {
    if (registry == null) {
      return;
    }

    Registry.Entry[] departures;

    if (time != null) {
      departures = registry.afterOrAt(time);
    } else {
      departures = registry.afterOrAt(LocalTime.MIN);
    }

    var fields = new ArrayList<String[]>();
    fields.add(new String[] {"Time", "Line", "Destination", "Track", "Delay", "#"});
    Arrays.stream(departures).map(entry -> {
      var dep = entry.departure();
      var delay = dep.getDelay().toString();
      var noDelay = delay.equals("00:00");
      return new String[] {
              dep.getTime().toString() + (noDelay ? "" : " (" + dep.getRealTime() + ")"),
              dep.getLine(),
              dep.getDestination(),
              dep.getTrack().map(Object::toString).orElse(""),
              delay.equals("00:00") ? "" : delay,
              Integer.toString(entry.number())
      };
    }).forEach(fields::add);

    var widest = new int[fields.get(0).length];
    for (var i = 0; i < widest.length; i++) {
      for (var field : fields) {
        widest[i] = Math.max(widest[i], field[i].length());
      }
    }

    var table = new StringBuilder();

    // top row
    var topRow = new StringBuilder("┌");
    for (var i = 0; i < widest.length; i++) {
      topRow.append("─".repeat(widest[i] + 2));
      if (i != widest.length - 1) { // don't add a cross after the last column
        topRow.append("┬");
      }
    }
    topRow.append("┐");
    table.append(topRow).append("\n").append("│ ");

    // headers
    var headers = fields.get(0);

    for (var i = 0; i < headers.length; i++) {
      var header = headers[i];
      table.append(header).append(" ".repeat(widest[i] - header.length())).append(" │ ");
    }
    table.append("\n");

    // cross row
    var crossRow = topRow.toString().replace("┌", "├").replace("┐", "┤").replace("┬", "┼");
    table.append(crossRow).append("\n");

    // departures
    for (var i = 1; i < fields.size(); i++) {
      var dep = fields.get(i);
      var row = new StringBuilder("│ ");
      for (var j = 0; j < dep.length; j++) {
        var field = dep[j];
        row.append(field).append(" ".repeat(widest[j] - field.length())).append(" │ ");
      }
      table.append(row).append("\n");
    }

    // bottom row
    var bottomRow = topRow.toString().replace("┌", "└").replace("┐", "┘").replace("┬", "┴");
    table.append(bottomRow).append("\n");
    System.out.println("\nCurrent time: " + time);
    System.out.println(table);
  }

  /**
   * Prints an exit message to the user.
   */
  public void printExitMessage() {
    System.out.println("Exiting...");
  }

  /**
   * Prints a welcome message to the user.
   */
  public void printWelcomeMessage() {
    System.out.println("\nWelcome to the train dispatch system.\n");
  }

  /**
   * Prompts the user for a time, and returns the time if it is valid.
   *
   * @return a valid time
   * @throws UserCancelException if the user cancels the command
   */
  public LocalTime promptTime() {
    while (true) {
      System.out.println("Enter a time (format: 'HH:MM'):");
      var time = getLine();
      try {
        return LocalTime.parse(time);
      } catch (DateTimeParseException e) {
        printCancelInfoWith("Invalid time: '" + time + "'");
      }
    }
  }

  /**
   * Prompts the user for an existing departure by number, and returns it.
   *
   * @return an existing departure
   * @throws UserCancelException if the user cancels the command
   * @throws IllegalArgumentException if the argument is null
   */
  public Registry.Entry promptExistingDeparture(Registry registry) {
    Parameter.notNull(registry, "registry");

    while (true) {

      System.out.println("Enter departure number:");
      var given = getLine();

      try {

        var number = Integer.parseInt(given);
        if (number > 0) {
          var maybeDep = registry.getDeparture(number);
          if (maybeDep.isPresent()) {
            return new Registry.Entry(number, maybeDep.get());
          } else {
            printCancelInfoWith("Departure " + number + " does not exist");
          }
        } else {
          printCancelInfoWith("Departure number must be positive");
        }
      } catch (NumberFormatException e) {
        printCancelInfoWith("Invalid departure number: '" + given + "'");
      }
    }
  }

  /**
   * Prompts the user for a new departure number, and returns it.
   *
   * @return a valid new (unused) departure number
   * @throws UserCancelException if the user cancels the command
   * @throws IllegalArgumentException if the argument is null
   */
  public int promptNewDepartureNumber(Registry registry) {
    Parameter.notNull(registry, "registry");

    while (true) {

      System.out.println("Enter departure number:");
      var given = getLine();

      try {

        var number = Integer.parseInt(given);
        if (number > 0) {
          if (registry.numbersInUse().contains(number)) {
            printCancelInfoWith("Departure " + number + " already exists");
          } else {
            return number;
          }
        } else {
          printCancelInfoWith("Departure number must be positive");
        }

      } catch (NumberFormatException e) {
        printCancelInfoWith("Invalid departure number: '" + given + "'");
      }
    }
  }

  /**
   * Prompts the user for a new departure line, and returns it.
   *
   * @return a valid new departure line
   * @throws UserCancelException if the user cancels the command
   * @throws IllegalArgumentException if either argument is null
   */
  public String promptLine(HashMap<LocalTime, String> timesToLines, LocalTime time) {
    Parameter.notNull(timesToLines, "timesToLines");
    Parameter.notNull(time, "time");

    var lineAtTime = timesToLines.get(time);
    while (true) {
      var line =  promptNonBlank("Enter a rail line:", "line");
      if (lineAtTime == null) {
        return line;
      } else if (lineAtTime.equals(line)) {
        printCancelInfoWith("There is already a departure at the same time with that line");
      } else {
        return line;
      }
    }
  }

  /**
   * Prompts the user for a departure line, and returns it.
   *
   * @return the departure given by the user
   * @throws UserCancelException if the user cancels the command
   */
  public String promptAnyLine() {
    return promptNonBlank("Enter a rail line:", "line");
  }

  /**
   * Prompts the user for a new departure destination, and returns it.
   *
   * @return the destination given by the user
   */
  public String promptDestination() {
    return promptNonBlank("Enter destination:", "destination");
  }

  /**
   * Prompts the user for a new departure track, and returns it.
   *
   * @return an optional representing whether the user entered a track, and if so, which one
   */
  public Optional<Integer> promptTrack() {
    return promptOptionalInt("Enter track number (or leave blank):", "track");
  }

  /**
   * Prompts the user for an integer,
   * returning and empty optional if the user leaves the input blank.
   *
   * @return an integer if one is entered, or an empty optional if the user leaves the input blank
   * @throws UserCancelException if the user cancels the command
   * @throws IllegalArgumentException if either argument is null
   */
  public Optional<Integer> promptOptionalInt(String prompt, String propertyName) {
    Parameter.notNull(prompt, "prompt");
    Parameter.notNull(propertyName, "propertyName");

    while (true) {
      System.out.println(prompt);
      var input = getLine();
      if (input.isBlank()) {
        return Optional.empty();
      } else {
        try {
          return Optional.of(Integer.parseInt(input));
        } catch (Exception e) {
          printCancelInfoWith("Invalid " + propertyName + ": '" + input + "'");
        }
      }
    }
  }

  private String promptNonBlank(String prompt, String propertyName) {
    Parameter.notNull(prompt, "prompt");
    Parameter.notNull(propertyName, "propertyName");

    while (true) {
      System.out.println(prompt);
      var input = getLine();
      if (input.isBlank()) {
        printCancelInfoWith(propertyName + " cannot be blank");
      } else {
        return input;
      }
    }
  }

  /**
   * Indicates to the user that no matches were found.
   */
  public void printNoMatches() {
    System.out.println("No matches found");
  }

  /**
   * Prints the given command history to the user in a presentable format.
   *
   * @param commandHistory the command history to print
   * @throws IllegalArgumentException if the argument is null
   */
  public void printCommandLog(ArrayList<CommandLog> commandHistory) {
    Parameter.notNull(commandHistory, "commandHistory");

    if (commandHistory.isEmpty()) {
      System.out.println("Command history is empty");
      return;
    }
    var out = new StringBuilder();
    for (var i = 0; i < commandHistory.size(); i++) {
      out.append((i + 1)).append(". ").append(commandHistory.get(i).display()).append("\n");
    }
    System.out.println(out);
  }

  /**
   * Indicates to the user that there are no commands to undo.
   */
  public void printNoUndoFeedback() {
    System.out.println("No commands to undo");
  }

  /**
   * Indicates to the user that the given time is earlier than the current time.
   *
   * @param current the current time
   * @throws IllegalArgumentException if the argument is null
   */
  public void printNonChronologyFeedback(LocalTime current) {
    Parameter.notNull(current, "current");
    printCancelInfoWith("New time must be after current time (current time is " + current + ")");
  }

  /**
   * Indicates to the user that the current command is being aborted.
   */
  public void printCancelMessage() {
    System.out.println("Command aborted");
  }
}
