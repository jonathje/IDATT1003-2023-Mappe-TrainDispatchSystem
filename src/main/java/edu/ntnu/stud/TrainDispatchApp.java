package edu.ntnu.stud;

import edu.ntnu.stud.commands.Add;
import edu.ntnu.stud.commands.Exit;
import edu.ntnu.stud.commands.FindByDest;
import edu.ntnu.stud.commands.FindByLine;
import edu.ntnu.stud.commands.FindByNumber;
import edu.ntnu.stud.commands.ListCommands;
import edu.ntnu.stud.commands.ListDepartures;
import edu.ntnu.stud.commands.Remove;
import edu.ntnu.stud.commands.SetClock;
import edu.ntnu.stud.commands.SetDelay;
import edu.ntnu.stud.commands.SetTrack;
import edu.ntnu.stud.commands.Tree;
import edu.ntnu.stud.commands.Undo;
import edu.ntnu.stud.validation.Parameter;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Optional;

/**
 * This is the main class for the train dispatch application.
 * It contains the main loop of the program.
 */
public class TrainDispatchApp {
  private Registry registry;
  private Ui userInterface;
  private ArrayList<CommandLog> commandHistory;
  private Command[] commands;
  private boolean running;
  private LocalTime clock;

  public static void main(String[] args) { // TODO
    TrainDispatchApp.init().start();
  }

  private TrainDispatchApp() {
    setRegistry(new Registry());
    setUserInterface(new Ui());

    setCommandHistory(new ArrayList<>());
    setCommands(new Command[]{
      new ListCommands(),
      new ListDepartures(),
      new FindByNumber(),
      new FindByDest(),
      new FindByLine(),

      new Add(),
      new Remove(),
      new SetTrack(),
      new SetDelay(),
      new SetClock(),

      new Undo(),
      new Tree(),
      new Exit()
    });

    setRunning(false);
    setClock(LocalTime.of(0, 0));
  }

  private static TrainDispatchApp init() {
    return new TrainDispatchApp();
  }

  private void start() {

    running = true;
    userInterface.printWelcomeMessage();
    userInterface.printCommands(commands);

    while (running) {

      var command = userInterface.promptCommand(commands);
      Optional<CommandLog> maybeLog = Optional.empty();

      try {
        maybeLog = command.execute(this);
      } catch (UserCancelException e) {
        userInterface.printCancelMessage();
      }

      maybeLog.ifPresent(log -> {
        commandHistory.add(log);
        userInterface.printDepartureList(registry, clock);
      });

    }

    userInterface.printExitMessage();
  }


  /**
   * Returns the departure registry.
   *
   * @return The registry
   */
  public Registry getRegistry() {
    return registry;
  }

  /**
   * Sets the departure registry to the given value.
   *
   * @throws IllegalArgumentException If the argument is null
   */
  public void setRegistry(Registry registry) {
    Parameter.notNull(registry, "registry");
    this.registry = registry;
  }

  /**
   * Returns the user interface.
   *
   * @return The user interface
   */
  public Ui getUserInterface() {
    return userInterface;
  }

  /**
   * Sets the user interface to the given value.
   *
   * @throws IllegalArgumentException If the argument is null
   */
  public void setUserInterface(Ui userInterface) {
    Parameter.notNull(userInterface, "userInterface");
    this.userInterface = userInterface;
  }

  /**
   * Returns the history of commands.
   *
   * @return The command history
   */
  public ArrayList<CommandLog> getCommandHistory() {
    return commandHistory;
  }

  /**
   * Sets the command history to the given value.
   *
   * @throws IllegalArgumentException If the list is null
   */
  private void setCommandHistory(ArrayList<CommandLog> commandHistory) {
    Parameter.notNull(commandHistory, "commandHistory");
    this.commandHistory = commandHistory;
  }

  /**
   * Returns the list of possible user commands.
   *
   * @return The commands
   */
  public Command[] getCommands() {
    return commands;
  }

  private void setCommands(Command[] commands) {
    Parameter.notNull(commands, "commands");
    this.commands = commands;
  }

  /**
   * Returns whether the application is running.
   * I.e. whether the main loop is going to keep going or break on the next iteration.
   *
   * @return True if the main loop will keep running, false otherwise
   */
  public boolean isRunning() {
    return running;
  }

  /**
   * Sets whether the application is running.
   * I.e. sets whether the main loop will break on the next iteration.
   * A value of true means the main loop will keep running,
   * a value of false means the main loop will break.
   */
  public void setRunning(boolean running) {
    this.running = running;
  }

  /**
   * Returns the current time set in the application.
   *
   * @return The current time
   */
  public LocalTime getClock() {
    return clock;
  }

  /**
   * Sets the current time in the application to the given value.
   *
   * @throws IllegalArgumentException If the given time is null
   */
  public void setClock(LocalTime clock) {
    Parameter.notNull(clock, "clock");
    this.clock = clock;
  }

  /**
   * Pops the last command log from the command history.
   *
   * @return The last command log, or empty if the history is empty
   */
  public Optional<CommandLog> popCommandHistory() {
    if (commandHistory.isEmpty()) {
      return Optional.empty();
    }
    var log = commandHistory.remove(commandHistory.size() - 1);
    return Optional.of(log);
  }
}
