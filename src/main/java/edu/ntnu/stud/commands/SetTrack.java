package edu.ntnu.stud.commands;

import edu.ntnu.stud.Command;
import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.commands.logs.SetTrackLog;
import java.util.Optional;

/**
 * Implements the logic for the 'track' command.
 */
public class SetTrack implements Command {
  @Override
  public Optional<CommandLog> execute(TrainDispatchApp app) {
    var ui = app.getUserInterface();
    var reg = app.getRegistry();

    var entry = ui.promptExistingDeparture(reg);
    var num = entry.number();
    var dep = entry.departure();
    var newTrack = ui.promptTrack();

    var previousTrack = entry.departure().getTrack();

    if (newTrack.isEmpty()) {
      dep.resetTrack();
    } else {
      dep.setTrack(newTrack.get());
    }

    var log = previousTrack.map(t -> new SetTrackLog(num, t)).orElseGet(() -> new SetTrackLog(num));
    return Optional.of(log);
  }

  @Override
  public String identifier() {
    return "track";
  }

  @Override
  public String description() {
    return "Set the track for a departure, or remove it if no track is specified";
  }
}
