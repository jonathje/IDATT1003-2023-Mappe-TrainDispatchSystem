package edu.ntnu.stud.commands;

import edu.ntnu.stud.Command;
import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.TrainDispatchApp;
import java.util.Optional;

/**
 * Implements the logic for the 'help' command.
 */
public class ListCommands implements Command {
  @Override
  public Optional<CommandLog> execute(TrainDispatchApp app) {
    app.getUserInterface().printCommands(app.getCommands());

    return Optional.empty();
  }

  @Override
  public String identifier() {
    return "help";
  }

  @Override
  public String description() {
    return "List all available commands";
  }
}