package edu.ntnu.stud.commands;

import edu.ntnu.stud.Command;
import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.Departure;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.commands.logs.AddLog;
import java.util.Optional;

/**
 * Implements the logic for the 'add' command.
 */
public class Add implements Command {
  @Override
  public Optional<CommandLog> execute(TrainDispatchApp app) {
    var registry = app.getRegistry();
    var ui = app.getUserInterface();
    var number = ui.promptNewDepartureNumber(registry);
    var time = ui.promptTime();
    var line = ui.promptLine(registry.timeLinePairs(), time);
    var destination = ui.promptDestination();
    var track = ui.promptTrack();

    var departure = new Departure(time, line, destination);
    track.ifPresent(departure::setTrack);

    registry.add(number, departure);

    return Optional.of(new AddLog(number));
  }

  @Override
  public String identifier() {
    return "add";
  }

  @Override
  public String description() {
    return "Add a new departure to the registry";
  }
}