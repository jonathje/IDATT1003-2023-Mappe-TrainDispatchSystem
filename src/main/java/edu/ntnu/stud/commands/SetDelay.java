package edu.ntnu.stud.commands;

import edu.ntnu.stud.Command;
import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.commands.logs.SetDelayLog;
import java.util.Optional;

/**
 * Implements the logic for the 'delay' command.
 */
public class SetDelay implements Command {
  @Override
  public Optional<CommandLog> execute(TrainDispatchApp app) {
    var ui = app.getUserInterface();
    var reg = app.getRegistry();

    var entry = ui.promptExistingDeparture(reg);
    var dep = entry.departure();
    var delay = ui.promptTime();

    var previousDelay = dep.getDelay();
    dep.setDelay(delay);

    return Optional.of(new SetDelayLog(entry.number(), previousDelay));
  }

  @Override
  public String identifier() {
    return "delay";
  }

  @Override
  public String description() {
    return "Set the delay for a departure";
  }
}
