package edu.ntnu.stud.commands;

import edu.ntnu.stud.Command;
import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.TrainDispatchApp;
import java.util.Optional;

/**
 * Implements the logic for the 'list' command.
 */
public class ListDepartures implements Command {
  @Override
  public Optional<CommandLog> execute(TrainDispatchApp app) {
    app.getUserInterface().printDepartureList(app.getRegistry(), app.getClock());

    return Optional.empty();
  }

  @Override
  public String identifier() {
    return "list";
  }

  @Override
  public String description() {
    return "List all departures currently entered in the registry";
  }
}