package edu.ntnu.stud.commands;

import edu.ntnu.stud.Command;
import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.commands.logs.RemoveLog;
import java.util.Optional;

/**
 * Implements the logic for the 'remove' command.
 */
public class Remove implements Command {
  @Override
  public Optional<CommandLog> execute(TrainDispatchApp app) {
    var ui = app.getUserInterface();
    var reg = app.getRegistry();

    var removed = ui.promptExistingDeparture(reg);

    reg.remove(removed.number());

    return Optional.of(new RemoveLog(removed));
  }

  @Override
  public String identifier() {
    return "remove";
  }

  @Override
  public String description() {
    return "Remove a departure from the registry";
  }
}
