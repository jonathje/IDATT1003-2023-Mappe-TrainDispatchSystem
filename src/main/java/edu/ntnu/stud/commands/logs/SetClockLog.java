package edu.ntnu.stud.commands.logs;

import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.validation.Parameter;
import java.time.LocalTime;

/**
 * Implements the logic for undoing the 'clock' command.
 */
public class SetClockLog implements CommandLog {
  private final LocalTime prev;


  /**
   * Creates a new instance.
   *
   * @param prev The previous clock time
   * @throws IllegalArgumentException If the argument is null
   */
  public SetClockLog(LocalTime prev) {
    Parameter.notNull(prev, "prev");

    this.prev = prev;
  }

  @Override
  public void undo(TrainDispatchApp app) {
    app.setClock(prev);
  }

  @Override
  public String display() {
    return "set clock";
  }

  private LocalTime getPrev() {
    return prev;
  }
}
