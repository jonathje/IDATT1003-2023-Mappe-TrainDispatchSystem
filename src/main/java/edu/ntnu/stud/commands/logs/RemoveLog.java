package edu.ntnu.stud.commands.logs;

import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.Registry;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.validation.Parameter;

/**
 * Implements the logic for undoing the 'remove' command.
 */
public class RemoveLog implements CommandLog {
  private final Registry.Entry entry;

  /**
   * Creates a new instance.
   *
   * @param departure The departure that was removed
   * @throws IllegalArgumentException If the argument is null
   */
  public RemoveLog(Registry.Entry departure) {
    Parameter.notNull(departure, "departure");

    this.entry = departure;
  }

  @Override
  public void undo(TrainDispatchApp app) {
    app.getRegistry().add(entry.number(), entry.departure());
  }

  @Override
  public String display() {
    return "removed departure " + entry.number();
  }

  private Registry.Entry getEntry() {
    return entry;
  }
}
