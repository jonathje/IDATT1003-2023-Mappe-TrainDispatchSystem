package edu.ntnu.stud.commands.logs;

import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.TrainDispatchApp;

/**
 * Implements the logic for undoing the 'add' command.
 */
public class AddLog implements CommandLog {
  private final int departure;

  public AddLog(int departure) {
    this.departure = departure;
  }

  @Override
  public void undo(TrainDispatchApp app) {
    app.getRegistry().remove(departure);
  }

  @Override
  public String display() {
    return "added departure " + departure;
  }

  private int getDeparture() {
    return departure;
  }
}
