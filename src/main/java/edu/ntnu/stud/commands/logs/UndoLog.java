package edu.ntnu.stud.commands.logs;

import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.commands.Undo;

/**
 * Implements 'undo' chaining. I.e. when an instance of this is "undone"
 * it will instead undo the previous command on the stack.
 */
public class UndoLog implements CommandLog {

  @Override
  public void undo(TrainDispatchApp app) {
    var log = new Undo().execute(app);
  }

  @Override
  public String display() {
    return "undid last";
  }
}
