package edu.ntnu.stud.commands.logs;

import edu.ntnu.stud.CommandLog;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.validation.Parameter;
import java.time.LocalTime;

/**
 * Implements the logic for undoing the 'delay' command.
 */
public class SetDelayLog implements CommandLog {
  private final int departure;
  private final LocalTime previousDelay;

  /**
   * Creates a new instance.
   *
   * @param departure The departure that was delayed
   * @param previousDelay The previous delay
   * @throws IllegalArgumentException If any of the previous delay is null
   */
  public SetDelayLog(int departure, LocalTime previousDelay) {
    Parameter.notNull(previousDelay, "previousDelay");

    this.departure = departure;
    this.previousDelay = previousDelay;
  }

  @Override
  public void undo(TrainDispatchApp app) {
    var departure = app.getRegistry().getDeparture(this.departure).orElseThrow();
    // assuming the order of undos is correct...
    departure.setDelay(previousDelay);
  }

  @Override
  public String display() {
    return "set delay of departure " + departure;
  }

  private int getDeparture() {
    return departure;
  }

  private LocalTime getPreviousDelay() {
    return previousDelay;
  }
}
