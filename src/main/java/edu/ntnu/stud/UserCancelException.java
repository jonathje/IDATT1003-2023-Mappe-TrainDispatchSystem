package edu.ntnu.stud;

/**
 * Indicates to the caller that the user cancelled the operation.
 */
public class UserCancelException extends RuntimeException {

}
