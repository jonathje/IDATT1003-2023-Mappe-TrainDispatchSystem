package edu.ntnu.stud;

/**
 * Represents the log resulting from a command. If the command is undoable,
 * the log must contain the information required to undo it.
 */
public interface CommandLog {
  void undo(TrainDispatchApp app);

  String display();
}
