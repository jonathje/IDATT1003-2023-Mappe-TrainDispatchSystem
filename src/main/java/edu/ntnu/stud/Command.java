package edu.ntnu.stud;

import java.util.Optional;

/**
 * Interface for commands that can be executed by the {@link TrainDispatchApp}.
 *
 * <p>The {@link #execute(TrainDispatchApp)} method should return an {@link Optional} containing a
 * {@link CommandLog} if the command was executed successfully, or an empty {@link Optional} if the
 * command failed.
 *
 * <p>The {@link #identifier} method should return a unique identifier for the command. This is used
 * to identify the command when the user enters a command in the console.
 *
 * <p>The {@link #description} method should return a short description of the command. This is used
 * to display a list of available commands to the user.
 */
public interface Command {
  Optional<CommandLog> execute(TrainDispatchApp app);

  String identifier();
  
  String description();
}
