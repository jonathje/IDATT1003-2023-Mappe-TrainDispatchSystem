# Portfolio project IDATA1003 - 2023

STUDENT NAME = "Jonathan Jensen"  
STUDENT ID = "111717"

## Project description

This is a program that can be used to organise, manipulate and display a list of departures from a train station.
It has some basic functionality, including:
- Adding a departure to the list
- Removing a departure from the list
- Displaying the list of departures
- Setting information about a departure, including track number and delay
- Finding specific departures based on their train number, destination or track number
- Displaying previous changes to the list
- Undoing previous changes to the list

## Project structure

The main source code is stored in the `main` folder, and the test code is stored in the `test` folder.
The code can be found in the `java.edu.ntnu.stud` package. The most important classes are stored in the package root,
while the `commands` package contains the classes for the commands that can be executed on the list of departures. The `validation` package provides some utility methods.

## Link to repository

https://gitlab.stud.idi.ntnu.no/jonathje/IDATT1003-2023-Mappe-TrainDispatchSystem

## How to run the project

What is the input and output of the program? What is the expected behaviour of the program?)
The simplest and easiest way to run the project is to open it in IntelliJ IDEA and build/run it with the built-in compiler.
Otherwise, you can compile and run it manually as described in this section.

The entry point is the `main` method in the `TrainDispatchApp` main class.

Make sure you have Maven and the JVM installed.

Compile the project using Maven with `mvn compile` from the project root. This will create a `target` folder with the compiled class files.

Run the project with `java edu.ntnu.stud.TrainDispatchApp` from the `target/classes` folder.
The program will guide you through the process of using commands to display and manipulate the list of departures.

## How to run the tests

Make sure you have Maven installed.

Tests can be run with `mvn test` from the project root.